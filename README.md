# MarketPlace Service

### Prerequisites

```
Node >= v12.18.0
Npm = 6.14.4
Typescript >= 3.9.3
Docker >= 2.3
```

## Installation

Install [Node.js](https://nodejs.org/en/), [Git](https://git-scm.com/) and then:

```sh
git clone https://gitlab.com/thatshailesh/marketplace.git
cd marketplace
npm install
npm install tsc -g
npm install apidoc -g
npm run dev
```

## Folder Structure
```
src
│   app.ts          # App entry point
└───api             # Express route controllers for all the endpoints of the app
└───config          # Environment variables and configuration related stuff
└───loaders         # Split the startup process into modules
└───models          # Database models
└───services        # All the business logic is here
└───types           # Type declaration files (d.ts) for Typescript
```

## API DOC

Generate API DOC
```
apidoc -i src/ -o apidoc/
```

API Doc will be generated inside `apidoc` directory

```
open apidoc/index.html
```

### Running with docker

```
docker-compose build
docker-compose up
```

## Author

- **Shailesh Shekhawat** - _Initial work_ - [Github](https://gitlab.com/thatshailesh/marketplace.git)

## License

This project is licensed under the MIT License
import { Router } from 'express';
import products from './routes/products';
import coupons from './routes/coupons';

export default (): Router => {
    const app = Router();
    products(app);
    coupons(app);

    return app;
};
import { Router, Request, Response, NextFunction }  from 'express';
import { celebrate, Joi, errors } from 'celebrate';

import { Container } from 'typedi';
import winston from 'winston';
import CouponService from '../../services/CouponService';
import { PaginOptions } from '../../interfaces/PagingOptions';

const route = Router();

export default (app: Router): void => {
    app.use('/coupons', route);

    /**
         * @api {get} /coupons Request Coupons
         * @apiVersion 1.0.0
         * @apiName GetAllCoupons
         * @apiGroup Coupon
         *
         *
         * @apiParam (Query Params) {String} pageNumber Page number
         * @apiParam (Query Params) {String} recordsPerPage Records per page
         * @apiParam (Query Params) {String} sort Sort records in asc or desc
         * 
         * @apiSuccess {Boolean} valid Validity of coupon.
         * @apiSuccess {String} coupon_code  Coupon Code.
         * @apiSuccess {Number} discount_amount  Discount Amount.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     [{
         *       "valid": trye,
         *       "coupon_code": "Doe",
         *       "discount_amount": 22
         *     }]
         *
         * @apiError InternalServerError Internal error message.
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "errors": {
         *          "message": "Some error"
         *       }
         *     }
    */

    route.get('/', async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');

        logger.debug('Calling get all coupons data endpoint');

        try {
            const couponServiceInstance = Container.get(CouponService);
            const { pageNumber = 0, recordsPerPage = 10, sort = 'descending' } = req.query;
            const pagingOption: PaginOptions = {
                pageNumber: parseInt(pageNumber as string, 10), 
                recordsPerPage: parseInt(recordsPerPage as string, 10),
                sort: sort as string,
            };
            const result = await couponServiceInstance.getAll(pagingOption);
            return res.status(200).json(result);
        }catch(err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    })

    /**
         * @api {post} /coupons/redeem Redeem Coupon
         * @apiVersion 1.0.0
         * @apiName RedeemCoupon
         * @apiGroup Coupon
         *
         *
         * @apiParam (Request body) {String} coupon_code Coupon code to redeem
         * 
         * @apiSuccess {Boolean} valid Validity of coupon.
         * @apiSuccess {String} coupon_code  Coupon Code.
         * @apiSuccess {Number} discount_amount  Discount Amount.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     [{
         *       "valid": trye,
         *       "coupon_code": "Doe",
         *       "discount_amount": 22
         *     }]
         *
         * @apiError InternalServerError Internal error message.
         * @apiError InvalidCouponError Coupon is invalid.
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "errors": {
         *          "message": "Some error"
         *       }
         *     }
    */

    route.post('/redeem', celebrate({
        body: Joi.object({
            code: Joi.string().min(3).required()
        })}),
        async (req: Request, res: Response, next: NextFunction) => {
            const logger: winston.Logger = Container.get('logger');
            logger.debug('Calling coupon redeem endpoint with body: %o', req.body );
            try {
                const couponServiceInstance = Container.get(CouponService);
                const { code } = req.body
                const result = await couponServiceInstance.redeemCoupon(code)
                return res.status(201).json(result);
            } catch (err) {
                logger.error('🔥 error: %o', err);
                return next(err);
            }

        }
    )

    route.use(errors());
}

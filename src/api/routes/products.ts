import { Router, Request, Response, NextFunction }  from 'express';
import { Container } from 'typedi';
import winston from 'winston';
import ProductService from '../../services/ProductService'
import { celebrate, Joi, errors } from 'celebrate';
import { PaginOptions } from '../../interfaces/PagingOptions';



export default (app: Router): void => {
    const route = Router();
    app.use('/products', route);

    /**
         * @api {post} /products/search Search Product
         * @apiVersion 1.0.0
         * @apiName SearchProduct
         * @apiGroup Product
         *
         *
         * @apiParam (Request Body) {String} keyword Search for text present in anywhere product document
         * 
         * @apiSuccess {String} title Title of Product.
         * @apiSuccess {String} description  Description About product.
         * @apiSuccess {Number} price  Product price.
         * @apiSuccess {Number} discount  Product discount.
         * @apiSuccess {String} short_description  Product short description.
         * @apiSuccess {String} tag  Product tag.
         * @apiSuccess {String} image_url  Product image url.
         * @apiSuccess {String} seller_name  Product Seller name.
         * @apiSuccess {String} seller_location  Product Seller location.
         * @apiSuccess {String} rating  Product Rating.
         * @apiSuccess {String} estimated_delivery  Product Estimated Delivery.
         * @apiSuccess {String[]} variant  Product variants.
         * @apiSuccess {String} category  Product category.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     [ {
                    "image_url": [
                        "//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258"
                        ],
                    "variant": [
                        "vnxEsoRV8v)dFa*"
                    ],
                    "_id": "5f09cbfb2f0418433e955b58",
                    "title": "Hattie",
                    "description": "Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.",
                    "price": -1,
                    "discount": 1,
                    "short_description": "Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.",
                    "tag": "chair",
                    "seller_name": "Eleanor Miles",
                    "seller_location": "IO",
                    "rating": 868,
                    "estimated_delivery": "11 days",
                    "category": "Furniture",
                    "createdAt": "2020-07-11T14:26:03.829Z",
                    "updatedAt": "2020-07-11T14:26:03.829Z"
                }]
         *
         * @apiError InternalServerError Internal error message.
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "errors": {
         *          "message": "Some error"
         *       }
         *     }
    */

    route.post('/search', 
        celebrate({
            body: {
                keyword: Joi.alternatives(
                    Joi.string(),
                    Joi.number()
                ).required()
            },
        }),

    async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');
        logger.debug('Calling product search endpoint with body: %o', req.body );
        try {
            const productServiceInstance = Container.get(ProductService);
            const { keyword } = req.body
            const result = await productServiceInstance.findProduct(keyword)
            return res.status(201).json(result);
        } catch (err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    })

    /**
         * @api {post} /products Create Products
         * @apiVersion 1.0.0
         * @apiName CreateProduct
         * @apiGroup Product
         *
         *
         * @apiParam (Request Body) {String} title Title of Product.pageNumber Page number
         * @apiParam (Request Body) {String} description  Description About product.
         * @apiParam (Request Body) {String} price  Product price.
         * @apiParam (Request Body) {String} discount  Product discount.
         * 
         * @apiSuccess {String} title Title of Product.
         * @apiSuccess {String} description  Description About product.
         * @apiSuccess {Number} price  Product price.
         * @apiSuccess {Number} discount  Product discount.
         * @apiSuccess {String} short_description  Product short description.
         * @apiSuccess {String} tag  Product tag.
         * @apiSuccess {String} image_url  Product image url.
         * @apiSuccess {String} seller_name  Product Seller name.
         * @apiSuccess {String} seller_location  Product Seller location.
         * @apiSuccess {String} rating  Product Rating.
         * @apiSuccess {String} estimated_delivery  Product Estimated Delivery.
         * @apiSuccess {String[]} variant  Product variants.
         * @apiSuccess {String} category  Product category.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     [ {"productData": {
                    "image_url": [
                        "//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258"
                        ],
                    "variant": [
                        "vnxEsoRV8v)dFa*"
                    ],
                    "_id": "5f09cbfb2f0418433e955b58",
                    "title": "Hattie",
                    "description": "Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.",
                    "price": -1,
                    "discount": 1,
                    "short_description": "Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.",
                    "tag": "chair",
                    "seller_name": "Eleanor Miles",
                    "seller_location": "IO",
                    "rating": 868,
                    "estimated_delivery": "11 days",
                    "category": "Furniture",
                    "createdAt": "2020-07-11T14:26:03.829Z",
                    "updatedAt": "2020-07-11T14:26:03.829Z"
                }}]
         *
         * @apiError InternalServerError Internal error message.
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "errors": {
         *          "message": "Some error"
         *       }
         *     }
    */
    
    route.post('/', 
        celebrate({
            body: Joi.object({
                title: Joi.string().required(),
                description: Joi.string().required(),
                price: Joi.number().required(),
                discount: Joi.number().required(),
            }),
        }), async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');
        logger.debug('Calling Sign-Up endpoint with body: %o', req.body );
        try {
            const productServiceInstance = Container.get(ProductService);
            const result = await productServiceInstance.createProduct(req.body)
            return res.status(201).json(result);
        } catch (err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    })

    /**
         * @api {get} /products Request Products
         * @apiVersion 1.0.0
         * @apiName GetAllProducts
         * @apiGroup Product
         *
         *
         * @apiParam (Query Params) {String} pageNumber Page number
         * @apiParam (Query Params) {String} recordsPerPage Records per page
         * @apiParam (Query Params) {String} sort Sort records in asc or desc
         * 
         * @apiSuccess {String} title Title of Product.
         * @apiSuccess {String} description  Description About product.
         * @apiSuccess {Number} price  Product price.
         * @apiSuccess {Number} discount  Product discount.
         * @apiSuccess {String} short_description  Product short description.
         * @apiSuccess {String} tag  Product tag.
         * @apiSuccess {String} image_url  Product image url.
         * @apiSuccess {String} seller_name  Product Seller name.
         * @apiSuccess {String} seller_location  Product Seller location.
         * @apiSuccess {String} rating  Product Rating.
         * @apiSuccess {String} estimated_delivery  Product Estimated Delivery.
         * @apiSuccess {String[]} variant  Product variants.
         * @apiSuccess {String} category  Product category.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *      "pageNumber": 0,
                "recordsPerPage": 10,
         *        data: [{
                    "image_url": [
                        "//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258"
                        ],
                    "variant": [
                        "vnxEsoRV8v)dFa*"
                    ],
                    "_id": "5f09cbfb2f0418433e955b58",
                    "title": "Hattie",
                    "description": "Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.",
                    "price": -1,
                    "discount": 1,
                    "short_description": "Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.",
                    "tag": "chair",
                    "seller_name": "Eleanor Miles",
                    "seller_location": "IO",
                    "rating": 868,
                    "estimated_delivery": "11 days",
                    "category": "Furniture",
                    "createdAt": "2020-07-11T14:26:03.829Z",
                    "updatedAt": "2020-07-11T14:26:03.829Z"
                }]}
         *
         * @apiError InternalServerError Internal error message.
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "errors": {
         *          "message": "Some error"
         *       }
         *     }
    */

    route.get('/', async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');

        logger.debug('Calling get all products data endpoint');

        try {
            const productServiceInstance = Container.get(ProductService);
            const { pageNumber = 0, recordsPerPage = 10, sort = 'descending', criteria = null } = req.query;
            const pagingOption: PaginOptions = {
                pageNumber: parseInt(pageNumber as string, 10), 
                recordsPerPage: parseInt(recordsPerPage as string, 10),
                sort: sort as string,
            };

            if (criteria) pagingOption.criteria = criteria as string;

            const result = await productServiceInstance.getAllProducts(pagingOption);
            return res.status(200).json(result);

        }catch(err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    })

    route.use(errors());
}

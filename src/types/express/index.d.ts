import { Document, Model } from 'mongoose';
import { Product as IProduct } from '../../interfaces/Product';
import { Coupon as ICoupon } from '../../interfaces/Coupon'
declare global {
  namespace Express {}

  namespace Models {
    export type ProductModel = Mode<IProduct & Document>;
    export type CouponModel = Mode<ICoupon & Document>;
  }
}

import 'reflect-metadata'; // We need this in order to use @Decorators
import express from 'express';
import Logger from '../src/loaders/logger';
import config from 'config';
import cors from 'cors';


async function startServer(): Promise<void> {
    const app = express();
   
    try {
        await require('./loaders').default(app);
    }catch(err) {
        Logger.error('🔥 error: %o', err);
    }
    const port = config.get('port');
    app.use(cors());
    app.listen(port, () => {
        Logger.info(`
          ################################################
          🛡️  Server listening on port: ${port} 🛡️ 
          ################################################
        `);
    });

    
}

startServer();
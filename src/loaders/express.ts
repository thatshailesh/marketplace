
import config from 'config';
import cors from 'cors';
import express, { Request, Response, NextFunction } from 'express';
import router from '../api';
// Creates and configures an ExpressJS web server.

class App {
    public express: express.Application;
    //Run configuration methods on the Express instance.
    constructor(app: express.Application) {
        this.express = app;
        this.middleware();
        this.routes();
         
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(cors());
        this.express.use(express.json());
        this.express.use(express.urlencoded({ extended: false }));
    }

    // Configure API endpoints.
    private routes(): void {
        const { prefix: apiPrefix } = config.get('api');
        this.express.use(apiPrefix, router());
        this.express.use((req, res, next) => {
            const err: any = new Error('Not Found');
            err.status = 404;
            next(err);
        });
        this.express.use((err: any, req: Request, res: Response, next: NextFunction) => {
            res.status(err.httpStatus || 500);
            res.json({
                errors: {
                    message: err.message,
                },
            });
        });
    }
}

export default App;
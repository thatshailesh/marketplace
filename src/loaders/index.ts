import config from 'config';

import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import express from 'express';
import mongooseLoader from './mongoose';
import Logger from './logger';
import {seedCoupons, seedProducts} from './seed';

export default async ( expressApp: express.Application): Promise<void> => {
    const db = await mongooseLoader();
    const productsCount = config.get('products-count') || 10;
    const couponsCount = config.get('coupons-count') || 10;

    Logger.info('✌️ DB loaded and connected!');
    const productModel = {
        name: 'productModel',
        // Notice the require syntax and the '.default'
        model: require('../models/Product').default,
    };

    const couponModel = {
        name: 'couponModel',
        model: require('../models/Coupon').default,
    };


    await dependencyInjectorLoader({
        db,
        models: [
            productModel,
            couponModel
        ]
    });

    Logger.info('✌️ Dependency Inject or loaded');

    await seedProducts(productModel.model, productsCount as number)
    await seedCoupons(couponModel.model, couponsCount as number)

    Logger.info('✌️ Data seeded');

    await new expressLoader(expressApp);
    Logger.info('✌️ Express loaded');
};
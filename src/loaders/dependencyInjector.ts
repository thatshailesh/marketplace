import { Container } from 'typedi';
import mongoose from 'mongoose';
import { Db } from 'mongodb';

import LoggerInstance from './logger';


export default ({db, models}: { db: Db; models: { name: string; model: mongoose.Document }[] } ): void => {
    try {
        models.forEach(m => {
            Container.set(m.name, m.model);
        });

        Container.set('dbInstance', db);
        Container.set('logger', LoggerInstance);
        
        LoggerInstance.info('✌️ loaders injected into container');

    } catch (e) {
        LoggerInstance.error('🔥 Error on dependency injector loader: %o', e);
        throw e;
    }
};

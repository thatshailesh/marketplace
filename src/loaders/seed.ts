import { Chance } from 'chance'

const chance = new Chance()

export const seedProducts = (productModel, total: number) => {
    const products = []
    for (let i = 0; i < total; i++) {
        const product = {
            title: chance.first(),
            description: chance.sentence(),
            price: chance.integer({ min: -20, max: 20 }),
            discount: chance.integer({ min: -20, max: 20 }),
            short_description: chance.sentence(),
            tag: chance.string(),
            image_url: [chance.avatar()],
            seller_name: chance.name(),
            seller_location: chance.country(),
            rating: chance.integer({ min: 10, max: 10000 }),
            estimated_delivery: `${chance.integer({min: 1, max: 30})} days`,
            variant: chance.string(),
            category: chance.string()
        }
        products.push(productModel.create(product))
    }

    return Promise.all(products)
}

export const seedCoupons = (couponModel, total: number) => {
    const coupons = []
    for (let i = 0; i < total; i++) {
        const coupon = {
            valid: chance.bool(),
            discount_amount: chance.integer({min: 5, max: 80}),
            coupon_code: chance.string()
        }

        coupons.push(couponModel.create(coupon))
    }

    return Promise.all(coupons)
}
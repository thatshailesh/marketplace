import mongoose from 'mongoose';
import { Db } from 'mongodb';
import config from 'config';

mongoose.set('useCreateIndex', true);
const defaultMongoHost = 'mongodb://mongo_db:27017/marketplace';
export default async (): Promise<Db> => {
    const { host =  defaultMongoHost } = config.get('mongodb');
    const connection = await mongoose.connect(host, 
        { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false } );
    return connection.connection.db;
};

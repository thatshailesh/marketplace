export interface Product {
    title: string;
    description: string;
    price: number;
    discount: number;
    short_description: string;
    tag: string;
    image_url: string;
    seller_name: string;
    seller_location: string;
    rating: number;
    estimated_delivery: string;
    variant: string[],
    category: string;
}
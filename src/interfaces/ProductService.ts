import { Product } from "./Product";
import { PaginOptions } from "./PagingOptions";
import { PaginatedResponse } from "./PaginatedResponse";

export interface ProductService {
    createProduct(product: Product);
    getAllProducts(pagingOptions: PaginOptions): Promise<PaginatedResponse>;
    findProduct(productId: string): Product;
}
import { PaginatedResponse } from "./PaginatedResponse";
import { PaginOptions } from "./PagingOptions";

export interface CouponService {
    getAll(paging: PaginOptions): Promise<PaginatedResponse>;
    redeemCoupon(code: string): Promise<any>;
}
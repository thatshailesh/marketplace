export interface Coupon {
    valid: boolean;
    discount_amount: number;
}
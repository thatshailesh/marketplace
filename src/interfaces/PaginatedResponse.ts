export interface PaginatedResponse {
    recordsPerPage: number;
    pageNumber: number;
    data: any[];
}
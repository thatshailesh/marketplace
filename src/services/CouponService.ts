import winston from 'winston';
import { Service, Inject } from 'typedi';
import { PaginOptions } from '../interfaces/PagingOptions';
import { PaginatedResponse } from '../interfaces/PaginatedResponse';
import {CouponService as ICouponService} from '../interfaces/CouponService'

@Service()
export default class CouponService implements ICouponService {
    @Inject('couponModel') private couponModel: Models.CouponModel
    @Inject('logger') private logger: winston.Logger;
    private mapping: any = {
        ascending: 1,
        descending: -1,
    }
    public async getAll(pagingOptions: PaginOptions): Promise<PaginatedResponse> {
        this.logger.silly('Find all coupons');
        const { recordsPerPage = 0, pageNumber = 0, sort = null }  = pagingOptions;

        if (pagingOptions.sort)
            pagingOptions.sort = this.mapping[pagingOptions.sort];

        const result = await this.couponModel
            .find()
            .sort({ _id: sort })
            .skip(pageNumber * recordsPerPage)
            .limit(recordsPerPage);
        
        return { pageNumber, recordsPerPage, data: result };
    }

    public async redeemCoupon(code: string) {
        this.logger.silly('Redeem coupon');

        const coupon = await this.couponModel.findOne({ coupon_code: code });
        if (!coupon) 
            throw new Error(`Coupon not found for code ${code}`);

        if (coupon.valid) {
            this.logger.silly('Redeeming coupon');
            return this.couponModel.findOneAndUpdate({ coupon_code: code }, { valid: false })
        }

        throw new Error('Coupon not valid')
    }
}


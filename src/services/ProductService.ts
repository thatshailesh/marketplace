import winston from 'winston';

import { Service, Inject } from 'typedi';
import { ProductService as IProductService } from '../interfaces/ProductService';
import { Product } from '../interfaces/Product';
import { PaginOptions } from '../interfaces/PagingOptions';
import { PaginatedResponse } from '../interfaces/PaginatedResponse';
@Service()
export default class ProductService implements IProductService {
    @Inject('productModel') private productModel: Models.ProductModel
    @Inject('logger') private logger: winston.Logger;

    private mapping: any = {
        ascending: 1,
        descending: -1,
    }

    public async createProduct(product: Product) {
        this.logger.silly('Create new product data');
        const productRecord = await this.productModel.create(product);

        if (!productRecord) 
            throw new Error('Product data cannot be created');

        const productData = productRecord.toObject();

        this.logger.silly('Db record created successfully');
        return { productData };
    }

    public async getAllProducts(pagingOptions: PaginOptions): Promise<PaginatedResponse> {
        const { recordsPerPage = 0, pageNumber = 0, sort = null }  = pagingOptions;

        this.logger.silly('Find all products');

        if (pagingOptions.sort)
            pagingOptions.sort = this.mapping[pagingOptions.sort];

        const result = await this.productModel
            .find()
            .sort({ _id: sort })
            .skip(pageNumber * recordsPerPage)
            .limit(recordsPerPage);
        
        return { pageNumber, recordsPerPage, data: result };
    }

    findProduct(keyword): Product {
        let query: any = {
            $text: {
                $search: keyword,
            },
        };
        return this.productModel.find(query)
    }
}
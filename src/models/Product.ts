import mongoose from 'mongoose';

const { Schema } = mongoose;

const productSchema = new Schema(
    {
        title: { type: String, required: true },
        description: { type: String, required: true, index: true },
        price: { type: Number, required: true },
        discount: { type: Number, required: true },
        short_description: { type: String },
        tag: {type: String},
        image_url: {type: Array},
        seller_name: {type: String},
        seller_location: {type: String},
        rating: { type: Number },
        estimated_delivery: { type: String },
        variant: {type: Array},
        category: { type: String }
    },
    {
        timestamps: { createdAt: 'createdAt' },
        toObject: {
            transform (doc, ret): void {
                delete ret.__v;
            },
        },
    }
);

productSchema.index(
    { 
        '$**': 'text'
    }, 
    { 
        'default_language': 'english',
    }    
)

export default mongoose.model('Product', productSchema);
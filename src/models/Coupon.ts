import mongoose from 'mongoose';

const { Schema } = mongoose;

const couponSchema = new Schema({
    valid: {type: Boolean, required: true},
    discount_amount: {type: String, required: true},
    coupon_code: { type: String, required: true }
},{
    timestamps: { createdAt: 'createdAt' },
    toObject: {
        transform (doc, ret): void {
            delete ret.__v;
        },
    },
})

export default mongoose.model('Coupon', couponSchema);
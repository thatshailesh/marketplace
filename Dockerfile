FROM node:12.2.0-alpine

WORKDIR /app

COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm install
RUN npm install tsc -g --silent

COPY . /app

RUN npm run build

EXPOSE 8080

ARG NODE_ENV=dev
ENV NODE_ENV=${NODE_ENV}

CMD [ "npm", "start" ]

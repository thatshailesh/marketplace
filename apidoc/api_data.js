define({ "api": [
  {
    "type": "get",
    "url": "/coupons",
    "title": "Request Coupons",
    "version": "1.0.0",
    "name": "GetAllCoupons",
    "group": "Coupon",
    "parameter": {
      "fields": {
        "Query Params": [
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "pageNumber",
            "description": "<p>Page number</p>"
          },
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "recordsPerPage",
            "description": "<p>Records per page</p>"
          },
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort records in asc or desc</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "valid",
            "description": "<p>Validity of coupon.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "discount_amount",
            "description": "<p>Discount Amount.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\n  \"valid\": trye,\n  \"coupon_code\": \"Doe\",\n  \"discount_amount\": 22\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Internal error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"errors\": {\n     \"message\": \"Some error\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/api/routes/coupons.ts",
    "groupTitle": "Coupon"
  },
  {
    "type": "post",
    "url": "/coupons/redeem",
    "title": "Redeem Coupon",
    "version": "1.0.0",
    "name": "RedeemCoupon",
    "group": "Coupon",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon code to redeem</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "valid",
            "description": "<p>Validity of coupon.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "discount_amount",
            "description": "<p>Discount Amount.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[{\n  \"valid\": trye,\n  \"coupon_code\": \"Doe\",\n  \"discount_amount\": 22\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Internal error message.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCouponError",
            "description": "<p>Coupon is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"errors\": {\n     \"message\": \"Some error\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/api/routes/coupons.ts",
    "groupTitle": "Coupon"
  },
  {
    "type": "post",
    "url": "/products",
    "title": "Create Products",
    "version": "1.0.0",
    "name": "CreateProduct",
    "group": "Product",
    "parameter": {
      "fields": {
        "Request Body": [
          {
            "group": "Request Body",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of Product.pageNumber Page number</p>"
          },
          {
            "group": "Request Body",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description About product.</p>"
          },
          {
            "group": "Request Body",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Product price.</p>"
          },
          {
            "group": "Request Body",
            "type": "String",
            "optional": false,
            "field": "discount",
            "description": "<p>Product discount.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of Product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description About product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Product price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>Product discount.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_description",
            "description": "<p>Product short description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Product tag.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image_url",
            "description": "<p>Product image url.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_name",
            "description": "<p>Product Seller name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_location",
            "description": "<p>Product Seller location.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Product Rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "estimated_delivery",
            "description": "<p>Product Estimated Delivery.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "variant",
            "description": "<p>Product variants.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Product category.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[ {\"productData\": {\n                \"image_url\": [\n                    \"//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258\"\n                    ],\n                \"variant\": [\n                    \"vnxEsoRV8v)dFa*\"\n                ],\n                \"_id\": \"5f09cbfb2f0418433e955b58\",\n                \"title\": \"Hattie\",\n                \"description\": \"Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.\",\n                \"price\": -1,\n                \"discount\": 1,\n                \"short_description\": \"Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.\",\n                \"tag\": \"chair\",\n                \"seller_name\": \"Eleanor Miles\",\n                \"seller_location\": \"IO\",\n                \"rating\": 868,\n                \"estimated_delivery\": \"11 days\",\n                \"category\": \"Furniture\",\n                \"createdAt\": \"2020-07-11T14:26:03.829Z\",\n                \"updatedAt\": \"2020-07-11T14:26:03.829Z\"\n            }}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Internal error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"errors\": {\n     \"message\": \"Some error\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/api/routes/products.ts",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products",
    "title": "Request Products",
    "version": "1.0.0",
    "name": "GetAllProducts",
    "group": "Product",
    "parameter": {
      "fields": {
        "Query Params": [
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "pageNumber",
            "description": "<p>Page number</p>"
          },
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "recordsPerPage",
            "description": "<p>Records per page</p>"
          },
          {
            "group": "Query Params",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort records in asc or desc</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of Product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description About product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Product price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>Product discount.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_description",
            "description": "<p>Product short description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Product tag.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image_url",
            "description": "<p>Product image url.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_name",
            "description": "<p>Product Seller name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_location",
            "description": "<p>Product Seller location.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Product Rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "estimated_delivery",
            "description": "<p>Product Estimated Delivery.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "variant",
            "description": "<p>Product variants.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Product category.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n \"pageNumber\": 0,\n            \"recordsPerPage\": 10,\n   data: [{\n                \"image_url\": [\n                    \"//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258\"\n                    ],\n                \"variant\": [\n                    \"vnxEsoRV8v)dFa*\"\n                ],\n                \"_id\": \"5f09cbfb2f0418433e955b58\",\n                \"title\": \"Hattie\",\n                \"description\": \"Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.\",\n                \"price\": -1,\n                \"discount\": 1,\n                \"short_description\": \"Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.\",\n                \"tag\": \"chair\",\n                \"seller_name\": \"Eleanor Miles\",\n                \"seller_location\": \"IO\",\n                \"rating\": 868,\n                \"estimated_delivery\": \"11 days\",\n                \"category\": \"Furniture\",\n                \"createdAt\": \"2020-07-11T14:26:03.829Z\",\n                \"updatedAt\": \"2020-07-11T14:26:03.829Z\"\n            }]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Internal error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"errors\": {\n     \"message\": \"Some error\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/api/routes/products.ts",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/products/search",
    "title": "Search Product",
    "version": "1.0.0",
    "name": "SearchProduct",
    "group": "Product",
    "parameter": {
      "fields": {
        "Request Body": [
          {
            "group": "Request Body",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>Search for text present in anywhere product document</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of Product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description About product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Product price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>Product discount.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_description",
            "description": "<p>Product short description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Product tag.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image_url",
            "description": "<p>Product image url.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_name",
            "description": "<p>Product Seller name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "seller_location",
            "description": "<p>Product Seller location.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Product Rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "estimated_delivery",
            "description": "<p>Product Estimated Delivery.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "variant",
            "description": "<p>Product variants.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Product category.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[ {\n                \"image_url\": [\n                    \"//www.gravatar.com/avatar/b0e991630af3326b742b2e1222cbb258\"\n                    ],\n                \"variant\": [\n                    \"vnxEsoRV8v)dFa*\"\n                ],\n                \"_id\": \"5f09cbfb2f0418433e955b58\",\n                \"title\": \"Hattie\",\n                \"description\": \"Nadusit fafmizeg wokelulo nijmiwofu fujnu agafhe cev lovis bekta wonaj jipbi uci emvewva mid ogekopmeg ciunne vu zecnu.\",\n                \"price\": -1,\n                \"discount\": 1,\n                \"short_description\": \"Favikno motpekat ozpifu cavlo sulikote nerzanpop garusuzu heupinid kablofvus sormici kedto vasrig zonjo wil govjav zar.\",\n                \"tag\": \"chair\",\n                \"seller_name\": \"Eleanor Miles\",\n                \"seller_location\": \"IO\",\n                \"rating\": 868,\n                \"estimated_delivery\": \"11 days\",\n                \"category\": \"Furniture\",\n                \"createdAt\": \"2020-07-11T14:26:03.829Z\",\n                \"updatedAt\": \"2020-07-11T14:26:03.829Z\"\n            }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Internal error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"errors\": {\n     \"message\": \"Some error\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/api/routes/products.ts",
    "groupTitle": "Product"
  }
] });

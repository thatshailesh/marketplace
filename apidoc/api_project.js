define({
  "title": "Custom apiDoc browser title",
  "url": "http://localhost:8080/api",
  "name": "marketplace-service",
  "version": "1.0.0",
  "description": "Market Place Service",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-07-11T14:48:00.682Z",
    "url": "http://apidocjs.com",
    "version": "0.23.0"
  }
});
